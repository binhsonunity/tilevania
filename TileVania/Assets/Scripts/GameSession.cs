using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameSession : MonoBehaviour
{
    [SerializeField] int playerLives = 3;
    [SerializeField] int score = 0;
    [SerializeField] TextMeshProUGUI livesText;
    [SerializeField] TextMeshProUGUI scoreText;
    private void Awake()
    {
        //FindObjectsOfType: o day se~ la` 1 mang?
        //kiem? tra xem co bao nhieu phien tro` choi,bao nhieu doi tuong thi` .Length
        int numGameSessions = FindObjectsOfType<GameSession>().Length; 
        //khi chung ta da an 1 dong` coin hay giet 1 enemy khi chung ta die thi` sau khi load lai. canh? thi` cac doi' tuong. cua? ma` chung ta 
        //da~ tac' dong. van~ bi. pha' huy? , va` ca? so mang. da~ bi. mat'
        if(numGameSessions > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            //khi chung ta load lai. canh? thi` dung` pha' huy? nhung~ thu' con` ton` tai.,giu~ nguyen so diem hay so mang. da~ bi. mat'
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        livesText.text = playerLives.ToString();
        scoreText.text = score.ToString();
    }


    //cai chet' cua? nguoi` choi
    public void ProcessPlayerDeath()
    {
        //neu nguoi` choi van~ con` song'
        if(playerLives > 1)
        {
            TakeLife();
        }
        else
        {
            //neu' nguoi` choi da~ dung` het' so mang. cua? minh`
            ResetGameSession();
        }
    }

     void ResetGameSession()
    {

        FindObjectOfType<ScenePersist>().ResetScenePersist();
        //load lai. toan` bo. canh? game dau` tien
        SceneManager.LoadScene(0);
        //pha' huy? het tat' ca? cac doi' tuong. o? canh? cu~ de? quay lai. tro` choi ban dau`
        Destroy(gameObject);
    }

    void TakeLife()
    {
        //neu nguoi` choi va chanm voi chuong' ngai. vat. hay enemy thi` tru` di mang. cua? nguoi` choi
        playerLives--;
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        //sau do' load. lai. canh? game
        SceneManager.LoadScene(currentSceneIndex);
        //cap nhat. lai. so' mang. hien? thi.
        livesText.text = playerLives.ToString();
    }

    public void AddScore(int pointsToAdd)
    {
        score += pointsToAdd;
        scoreText.text = score.ToString();
    }
}
